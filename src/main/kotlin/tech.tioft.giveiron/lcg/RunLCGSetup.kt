package tech.tioft.giveiron.lcg

import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.block.Sign
import org.bukkit.command.BlockCommandSender
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender


class RunLCGSetup : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        if (sender !is BlockCommandSender) {
            return false
        }

        val senderLocation = sender.block.location

        val variables = getVariables(senderLocation)

        val lcg = createLinearCongruentialGenerator(variables[0], variables[1], variables[2], variables[3]).iterator()

        for (i in 0 until variables[4]) {
            Bukkit.broadcastMessage("x_${i+1} = ${lcg.next()}")
        }

        return true
    }

    private fun getVariables(commandBlockLocation: Location) : List<Int> {
        val res = ArrayList<Int>()
        val loc = commandBlockLocation.clone().add(3.0, 0.0, 0.0)
        for (i in 0 until 5) {
            val signLoc = loc.clone().add(i*5.0, 0.0, 0.0)
            val signBlock = signLoc.block
            val sign = signBlock.state as Sign
            res.add(sign.getLine(0).toInt())
        }
        return res
    }




}