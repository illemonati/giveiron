package tech.tioft.giveiron.setimmortal

import org.bukkit.entity.Entity
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageEvent

class ImmortalHandler : Listener {
    @EventHandler
    fun onDamage(evt: EntityDamageEvent) {
        val entity = evt.entity
        if (entity is Player) {
            if (immortalsContains(entity)) {
                evt.isCancelled = true
            }
        }
    }
}
