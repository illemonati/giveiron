package tech.tioft.giveiron.instantkill

import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class GiveInstantKill : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player) return false
        if (sender.inventory.itemInMainHand.type != Material.GOLDEN_HOE) {
            sender.sendMessage("You must have a golden hoe in your main hand!")
            return false
        }
        val meta = sender.inventory.itemInMainHand.itemMeta ?: return false
        meta.addEnchant(instantKill!!, 1, true)
        meta.setDisplayName("Staff of World Creation")
        sender.inventory.itemInMainHand.itemMeta = meta
        return true
    }

}