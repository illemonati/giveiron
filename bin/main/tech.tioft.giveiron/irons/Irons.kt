package tech.tioft.giveiron.irons

import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

class Irons : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player)
            return false
        val irons = ItemStack(Material.IRON_BLOCK, 64)
        sender.inventory.addItem(irons)

        return true
    }
}