package tech.tioft.giveiron.lcg

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import kotlin.random.Random

class LCGenerate : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        val a : Int;
        val c : Int;
        val m : Int;
        val x0 : Int;
        val amount: Int;


        when (args.size) {
            0 -> {
                a = Random.nextInt(0, 10000)
                c = Random.nextInt(0, 10000)
                m = Random.nextInt(0, 10000)
                x0 = Random.nextInt(0, 10000)
                amount = Random.nextInt(10)
            }
            5 -> {
                a = args[0].toInt()
                c = args[1].toInt()
                m = args[2].toInt()
                x0 = args[3].toInt()
                amount = args[4].toInt()
            }
            else -> {
                return false
            }
        }

        Bukkit.broadcastMessage("Generating LCG with a: $a, c: $c, m: $m, x0: $x0, for $amount terms")


        val LCG = createLinearCongruentialGenerator(a, c, m, x0).iterator()
        for (i in 0 until amount) {
            Bukkit.broadcastMessage("x_${i+1} = ${LCG.next()}")
        }

        return true
    }

}