package tech.tioft.giveiron.setimmortal

import org.bukkit.entity.Player

import java.util.ArrayList

private val players = ArrayList<Int>()

fun immortalsContains(player: Player): Boolean {
    return players.contains(player.entityId)
}

fun immortalsAdd(player: Player) {
    players.add(player.entityId)
}

fun immortalsRemove(player: Player) {
    if (players.contains(player.entityId)) {
        players.removeAt(player.entityId)
    }
}

