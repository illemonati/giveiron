package tech.tioft.giveiron.summonguards

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player

class SummonGuards : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player) return false
        if (!sender.isOp()) return false

        val loc1 = sender.location
        val loc2 = sender.location
        val loc3 = sender.location
        val loc4 = sender.location

        loc1.x += 5
        loc2.x -= 5
        loc3.z += 5
        loc4.z -= 5

        val locations = arrayOf(loc1, loc2, loc3, loc4)

        for (loc in locations) {
            sender.world.spawnEntity(loc, EntityType.IRON_GOLEM)
        }

        return true
    }
}