package tech.tioft.giveiron.apples

import org.bukkit.Material
import org.bukkit.NamespacedKey
import org.bukkit.Server
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.ShapedRecipe
import org.bukkit.plugin.Plugin

fun registerApplesRecipe(plugin: Plugin) {
    val apples = ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 64)
    val applesRecipe = ShapedRecipe(NamespacedKey(plugin, "golden_apples_stack"),apples)
    applesRecipe.shape("iii", "iag", "igg")
    applesRecipe.setIngredient('i', Material.IRON_BLOCK)
    applesRecipe.setIngredient('a', Material.APPLE)
    applesRecipe.setIngredient('g', Material.GOLD_BLOCK)
    plugin.server.addRecipe(applesRecipe)
}