package tech.tioft.giveiron

import tech.tioft.giveiron.instantkill.GiveInstantKill
import tech.tioft.giveiron.instantkill.InstantKill
import tech.tioft.giveiron.instantkill.InstantKillHandler
import tech.tioft.giveiron.instantkill.registerInstantKill
import tech.tioft.giveiron.irons.Irons
import org.bukkit.plugin.java.JavaPlugin
import tech.tioft.giveiron.apples.registerApplesRecipe
import tech.tioft.giveiron.die.Die
import tech.tioft.giveiron.givegodset.GiveGodSet
import tech.tioft.giveiron.givenightvision.GiveNightVision
import tech.tioft.giveiron.lcg.CreateLCGSetup
import tech.tioft.giveiron.lcg.LCGenerate
import tech.tioft.giveiron.lcg.RunLCGSetup
import tech.tioft.giveiron.motd.MotdHandler
import tech.tioft.giveiron.motd.SetMotd
import tech.tioft.giveiron.motd.SetSubMotd
import tech.tioft.giveiron.pointcompass.PointCompass
import tech.tioft.giveiron.pointcompass.PointCompassHandler
import tech.tioft.giveiron.rule30.GenerateRule30
import tech.tioft.giveiron.setborn.BornHandler
import tech.tioft.giveiron.setborn.SetBorn
import tech.tioft.giveiron.setimmortal.ImmortalHandler
import tech.tioft.giveiron.setimmortal.SetImmortal
import tech.tioft.giveiron.summonguards.SummonGuards

class GiveIron : JavaPlugin() {
    override fun onEnable() {
        this.getCommand("irons")!!.setExecutor(Irons())
        this.getCommand("setborn")!!.setExecutor(SetBorn())
        this.getCommand("die")!!.setExecutor(Die())
        this.getCommand("makeimmortal")!!.setExecutor(SetImmortal())
        this.getCommand("givenightvision")!!.setExecutor(GiveNightVision())
        this.getCommand("summonguards")!!.setExecutor(SummonGuards())
        this.getCommand("setmotd")!!.setExecutor(SetMotd())
        this.getCommand("setsubmotd")!!.setExecutor(SetSubMotd())
        this.getCommand("pointcompass")!!.setExecutor(PointCompass())
        this.getCommand("giveinstantkill")!!.setExecutor(GiveInstantKill())
        this.getCommand("givegodset")!!.setExecutor(GiveGodSet())
        this.getCommand("lcgenerate")!!.setExecutor(LCGenerate())
        this.getCommand("createlcgsetup")!!.setExecutor(CreateLCGSetup())
        this.getCommand("runlcgsetup")!!.setExecutor(RunLCGSetup())
        this.getCommand("generaterule30")!!.setExecutor(GenerateRule30())
        server.pluginManager.registerEvents(MotdHandler(), this)
        server.pluginManager.registerEvents(BornHandler(), this)
        server.pluginManager.registerEvents(ImmortalHandler(), this)
        server.pluginManager.registerEvents(PointCompassHandler(), this)
        server.pluginManager.registerEvents(InstantKillHandler(), this)
        registerInstantKill(this)
        registerApplesRecipe(this)
        println("GiveIron Enabled")
    }

    override fun onDisable() {
        super.onDisable()
    }
}