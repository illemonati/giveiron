package tech.tioft.giveiron.lcg

fun createLinearCongruentialGenerator(a: Int, c: Int, m: Int, x0: Int) = sequence {
    var x = x0
    while (true) {
        x = (a * x + c) % m
        yield(x)
    }
}

