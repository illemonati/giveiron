package tech.tioft.giveiron.instantkill

import org.bukkit.Material
import org.bukkit.NamespacedKey
import org.bukkit.enchantments.Enchantment
import org.bukkit.enchantments.EnchantmentTarget
import org.bukkit.inventory.ItemStack
import org.bukkit.plugin.Plugin

class InstantKill(namespacedKey: NamespacedKey) : Enchantment(namespacedKey) {

    override fun canEnchantItem(item: ItemStack): Boolean =
        item.type == Material.GOLDEN_HOE

    override fun getItemTarget(): EnchantmentTarget =
        EnchantmentTarget.TOOL

    override fun getName(): String =
        "InstantKill"

    override fun isCursed(): Boolean =
        false

    override fun isTreasure(): Boolean =
        false

    override fun getMaxLevel(): Int =
        1

    override fun getStartLevel(): Int =
        1

    override fun conflictsWith(other: Enchantment): Boolean =
        false

}

var instantKill : InstantKill? = null

fun registerInstantKill(plugin: Plugin) {
    try {
        val f = Enchantment::class.java.getDeclaredField("acceptingNew")
        f.isAccessible = true
        f.set(null, true)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    try {
        instantKill =
            InstantKill(NamespacedKey(plugin, "InstantKill1231"))
        Enchantment.registerEnchantment(instantKill!!)
    } catch (e: IllegalArgumentException) {
    } catch (e: Exception) {
        e.printStackTrace()
    }
}