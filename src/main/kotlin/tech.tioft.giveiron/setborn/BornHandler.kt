package tech.tioft.giveiron.setborn

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerRespawnEvent

class BornHandler : Listener {
    @EventHandler
    fun onPlayerRespawn(evt: PlayerRespawnEvent) {
        val location = getBornPoint(evt.player)
        println(location)
        if (location != null) {
            evt.respawnLocation = location
        }
    }
}