package tech.tioft.giveiron.instantkill

import org.bukkit.entity.Damageable
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageByEntityEvent

class InstantKillHandler : Listener {
    @EventHandler
    fun onEntityDamageByEntity(evt: EntityDamageByEntityEvent) {
        val damager = evt.damager
        val damaged = evt.entity
        if (damager !is Player || damaged !is Damageable) return
        if (!damager.inventory.itemInMainHand.enchantments.containsKey(instantKill)) return
        damaged.health = 0.0
    }
}