package tech.tioft.giveiron.rule30

import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import java.lang.NumberFormatException


class GenerateRule30 : CommandExecutor {


    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player) {
            sender.sendMessage("Sender Must be Player")
            return false
        }

        if (args.size != 2) {
            sender.sendMessage("Must have one argument for height and one argument for type")
            return false
        }

        val height : Int
        val type = args[1].toUpperCase()

        try {
            height = args[0].toInt()
        } catch (e: NumberFormatException) {
            sender.sendMessage("height could not be parsed")
            return false
        }

        val location = sender.getTargetBlock(null, 100).location


        when (type) {
            "2D" -> {
                buildRule30Display(location, height)
            }
            "3D" -> {
                build3DRule30Display(location, height)
            }
            else -> {
                sender.sendMessage("That is not a valid mode")
                return false
            }
        }



        return true
    }

    private fun buildRule30Display(location: Location, height: Int) {
        val width = 1000
        var line = Array<Int>(width) {_ -> 0}
        line[width/2] = 1

        drawLine(line, 0, location, height)

        for (i in 1..height) {
            line = getNextLine(line)
            drawLine(line, i, location, height)
        }
    }

    private fun build3DRule30Display(location: Location, height: Int) {
        val width = 1000
        var line = Array<Int>(width) {_ -> 0}
        line[width/2] = 1

        drawLine(line, 0, location, height)

        for (i in 0 until 4) {
            line = Array<Int>(width) {_ -> 0}
            line[width/2] = 1
            for (j in 1..height) {
                line = getNextLine(line)
                drawLine3D(line, j, location, height, i)
            }
        }

    }

    private fun getNextLine(line: Array<Int>) : Array<Int> =
        Array(line.size) { i: Int ->
            val top = line.getOrElse(i) {0}
            val right = line.getOrElse(i+1) {0}
            val left = line.getOrElse(i-1) {0}

            if (top != 1 && right != 1) {
                left
            } else {
                1 - left
            }
        }


    private fun drawLine(line: Array<Int>, y: Int, centerLocation: Location, height: Int) {
        val startingLocation = centerLocation.clone().add(
            -line.size/2.0,
            height.toDouble()-y,
            0.0
        )
        for (i in line.indices) {
            if (line[i] == 0) {
                continue
            }
            val loc = startingLocation.clone().add(i.toDouble(), 0.0, 0.0)
            loc.block.type = Material.PURPLE_WOOL
        }

    }

    private fun drawLine3D(line: Array<Int>, y: Int, centerLocation: Location, height: Int, face: Int) {
        val startingLocation = when (face) {
            0 -> {
                centerLocation.clone().add(
                    -line.size/2.0,
                    height.toDouble()-y,
                    0.0
                )
            }
            1 -> {
                centerLocation.clone().add(
                    line.size/2.0,
                    height.toDouble()-y,
                    0.0
                )
            }
            2 -> {
                centerLocation.clone().add(
                    0.0,
                    height.toDouble()-y,
                    -line.size/2.0
                )
            }
            3 -> {
                centerLocation.clone().add(
                    0.0,
                    height.toDouble()-y,
                    line.size/2.0
                )
            }
            else -> {
                return
            }
        }



        for (i in line.indices) {
            if (line[i] == 0) {
                continue
            }
            when (face) {
                0 -> {
                    startingLocation.clone().add(i.toDouble(), 0.0, 1.0*y).block.type = Material.MAGENTA_WOOL
                }
                1 -> {
                    startingLocation.clone().add(-i.toDouble(), 0.0, -1.0*y).block.type = Material.CYAN_WOOL
                }
                2 -> {
                    startingLocation.clone().add(1.0*y, 0.0, i.toDouble()).block.type = Material.LIME_WOOL
                }
                3 -> {
                    startingLocation.clone().add(-1.0*y, 0.0, -i.toDouble()).block.type = Material.ORANGE_WOOL
                }
                else -> {
                    return
                }
            }
        }

    }

}




