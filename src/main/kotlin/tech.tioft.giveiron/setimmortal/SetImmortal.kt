package tech.tioft.giveiron.setimmortal


import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player

class SetImmortal : CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<String>): Boolean {
        var playerToImmortal: Player? = null
        if (sender is ConsoleCommandSender) {
            if (args.size != 1) {
                sender.sendMessage("You must provide a single argument <player>")
                return false
            } else {
                playerToImmortal = Bukkit.getPlayer(args[0])
            }
        } else if (sender is Player) {
            if (!sender.isOp) {
                sender.sendMessage("You must be an op to use this command")
                return false
            }
            playerToImmortal = when {
                args.isEmpty() -> sender
                args.size == 1 -> Bukkit.getPlayer(args[0])
                else -> {
                    sender.sendMessage("You must provide a single argument <player>")
                    return false
                }
            }
        }

        if (playerToImmortal == null) {
            sender.sendMessage("That didn't work")
            return false
        }

        immortalsAdd(playerToImmortal)
        Bukkit.broadcastMessage(String.format("%s has been made immortal", playerToImmortal.toString()))

        return true
    }
}

