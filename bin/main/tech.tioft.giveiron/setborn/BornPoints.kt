package tech.tioft.giveiron.setborn

import org.bukkit.Location
import org.bukkit.entity.Player

var bornpoints = HashMap<Int, Location> ()

fun setBornPoint(player: Player, location: Location) {
    bornpoints[player.entityId] = location
}

fun clearBornPoint(player: Player) {
    bornpoints.remove(player.entityId)
}

fun getBornPoint(player: Player) =
    bornpoints[player.entityId]
