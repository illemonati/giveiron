package tech.tioft.giveiron.setborn

import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class SetBorn : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player) return false
        val location = sender.location
        setBornPoint(sender, location)
        sender.sendMessage("Spawn point set at :\n$location")
        return true
    }

}