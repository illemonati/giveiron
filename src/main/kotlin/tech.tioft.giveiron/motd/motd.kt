package tech.tioft.giveiron.motd

import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

var motd = "Welcome"
var submotd = "to this server"

class SetMotd: CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (!sender.isOp) return false;
        motd = args.joinToString(" ")
        sender.sendMessage("motd set to \n$motd")
        return true
    }
}

class SetSubMotd: CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (!sender.isOp) return false;
        submotd = args.joinToString(" ")
        sender.sendMessage("submotd set to \n$submotd")
        return true
    }
}

class MotdHandler: Listener {
    @EventHandler
    fun onPlayerJoin(evt: PlayerJoinEvent) {
        evt.player.sendTitle(motd, submotd, 20, 20*5, 20)
    }
}