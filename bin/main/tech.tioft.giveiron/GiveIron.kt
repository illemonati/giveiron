package tech.tioft.giveiron

import tech.tioft.giveiron.irons.Irons
import org.bukkit.plugin.java.JavaPlugin
import tech.tioft.giveiron.die.Die
import tech.tioft.giveiron.givenightvision.GiveNightVision
import tech.tioft.giveiron.motd.MotdHandler
import tech.tioft.giveiron.motd.SetMotd
import tech.tioft.giveiron.motd.SetSubMotd
import tech.tioft.giveiron.setborn.BornHandler
import tech.tioft.giveiron.setborn.SetBorn
import tech.tioft.giveiron.setimmortal.ImmortalHandler
import tech.tioft.giveiron.setimmortal.SetImmortal
import tech.tioft.giveiron.summonguards.SummonGuards

class GiveIron : JavaPlugin() {
    override fun onEnable() {
        this.getCommand("irons")!!.setExecutor(Irons())
        this.getCommand("setborn")!!.setExecutor(SetBorn())
        this.getCommand("die")!!.setExecutor(Die())
        this.getCommand("makeimmortal")!!.setExecutor(SetImmortal())
        this.getCommand("givenightvision")!!.setExecutor(GiveNightVision())
        this.getCommand("summonguards")!!.setExecutor(SummonGuards())
        this.getCommand("setmotd")!!.setExecutor(SetMotd())
        this.getCommand("setsubmotd")!!.setExecutor(SetSubMotd())
        server.pluginManager.registerEvents(MotdHandler(), this)
        server.pluginManager.registerEvents(BornHandler(), this)
        server.pluginManager.registerEvents(ImmortalHandler(), this)

        println("GiveIron Enabled")
    }

    override fun onDisable() {
        super.onDisable()
    }
}