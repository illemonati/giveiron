package tech.tioft.giveiron.lcg

import org.bukkit.Location


data class SignLocation(val variableLocation: Location, val variableName: String) {
    val inputValueLocation = variableLocation.clone().add(1.0, 0.0, 0.0)
}

