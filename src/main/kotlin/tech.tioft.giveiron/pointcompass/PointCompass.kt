package tech.tioft.giveiron.pointcompass

import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.CompassMeta
import java.util.*


var compassPlayers = mutableMapOf<UUID, UUID>()


class PointCompass : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player) {
            sender.sendMessage("This command is for players only")
            return false
        }
        if (args.size != 1) {
            sender.sendMessage("This command require exactly one argument being the player you wish to point your compass to")
            sender.sendMessage("You could also put \"clear\" as a argument to reset your compass location")
            return false
        }
        if (args[0] == "clear") {
            compassPlayers.remove(sender.uniqueId)
            sender.compassTarget = sender.world.spawnLocation
            sender.sendMessage("Compass cleared!")
            giveCompass(sender)
            return true
        }
        val playerToPointCompass = Bukkit.getPlayer(args[0])
        if (playerToPointCompass == null) {
            sender.sendMessage("There was a error parsing the user you have inputted!")
            sender.sendMessage("Please check the username and try again!")
            return false
        }
        compassPlayers[sender.uniqueId] = playerToPointCompass.uniqueId
        sender.compassTarget = playerToPointCompass.location
        sender.sendMessage("Compass now points to the location of ${playerToPointCompass.name}!")
        giveCompass(sender)

        return true
    }

    fun giveCompass(player: Player) {
        if (player.inventory.contains(Material.COMPASS)) return
        player.inventory.addItem(ItemStack(Material.COMPASS, 1))
        player.sendMessage("An compass has been given to you!")
    }
}

class PointCompassHandler : Listener {
    @EventHandler
    fun onPlayerMove(evt: PlayerMoveEvent) {
        val filteredMap = compassPlayers.filterValues { it == evt.player.uniqueId }
        filteredMap.forEach { k, v ->
            val pointingPlayer = Bukkit.getPlayer(k)
            val pointToPlayer = Bukkit.getPlayer(v)
            if (pointingPlayer == null || pointToPlayer == null) return@forEach
            pointingPlayer.compassTarget = pointToPlayer.location
            val compasses = pointingPlayer.inventory.all(Material.COMPASS)
            for ((_, compass) in compasses) {
                val meta = compass.itemMeta;
                if (meta !is CompassMeta) continue
                meta.lodestone = pointToPlayer.location
                meta.isLodestoneTracked = false
                compass.itemMeta = meta
            }
        }
    }
}
