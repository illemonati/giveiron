package tech.tioft.giveiron.lcg

import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.World
import org.bukkit.block.CommandBlock
import org.bukkit.block.Sign
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import kotlin.random.Random


class CreateLCGSetup : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        if (sender !is Player) {
            sender.sendMessage("This command is only callable as Player !")
            return false
        }

        val senderLocation = sender.location
        senderLocation.x = senderLocation.blockX.toDouble()
        senderLocation.y = senderLocation.blockY.toDouble()
        senderLocation.z = senderLocation.blockZ.toDouble()


        val senderWorld = sender.world

        createSetup(senderLocation, senderWorld)

        return true
    }


    private fun createSetup(location: Location, world: World) {
        location.add(0.0, 0.0, -5.0)
        val variablesNames = listOf("a", "c", "m", "x0", "amount")
        createSigns(location, world, variablesNames)
        createCommandBlock(location.clone().add(-2.0, 0.0, 0.0))

    }


    private fun createCommandBlock(location: Location) {
        val commandBlockBlock = location.block
        commandBlockBlock.type = Material.COMMAND_BLOCK
        val commandBlock = commandBlockBlock.state as CommandBlock
        commandBlock.setCommand("runlcgsetup")
        commandBlock.update()

        val commandBlockButtonLoc = location.clone().add(0.0, 0.0, 1.0)
        val commandBlockButtonBlock = commandBlockButtonLoc.block
        commandBlockButtonBlock.type = Material.STONE_BUTTON

    }


    private fun createSigns(location: Location, world: World, variablesNames: List<String>){
        val signLocations = ArrayList<SignLocation>();
        for ((i, variableName) in variablesNames.withIndex()) {
            val loc = location.clone()
            loc.x += i * 5
            val signLocation = SignLocation(loc, variableName)
            signLocations.add(signLocation)


            val vblock = world.getBlockAt(signLocation.variableLocation)
            vblock.type = Material.OAK_SIGN
            (vblock.state as Sign).setLine(0, variableName)
            val vblockSign = vblock.state as Sign
            vblockSign.setLine(0, variableName)
            vblockSign.update()

            val iblock = world.getBlockAt(signLocation.inputValueLocation)
            iblock.type = Material.OAK_SIGN
            val iblockSign = iblock.state as Sign
            iblockSign.setLine(0, Random.nextInt(0, 10000).toString())
            iblockSign.update()
        }

    }




}