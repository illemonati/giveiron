package tech.tioft.giveiron.givegodset

import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.enchantments.Enchantment
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.PotionMeta
import org.bukkit.potion.PotionData
import org.bukkit.potion.PotionType
import java.lang.IllegalArgumentException

class GiveGodSet : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        var playerToGodSet: Player? = null
        when (sender) {
            is ConsoleCommandSender -> {
                if (args.size != 1) {
                    sender.sendMessage("You need exactly one argument, <player>")
                    return false
                }
                playerToGodSet = Bukkit.getPlayer(args[0])
            }
            is Player -> {
                if (!sender.isOp) {
                    sender.sendMessage("You must be op to use this command!")
                    return false
                }
                when {
                    args.isEmpty() -> playerToGodSet = sender
                    args.size == 1 -> playerToGodSet = Bukkit.getPlayer(args[0])
                    else -> {
                        sender.sendMessage("You need exactly 0 or 1 arguments")
                    }
                }
            }
        }

        if (playerToGodSet == null) {
            sender.sendMessage("That did not work, please check your input!")
            return false
        }

        givePlayerGodSet(playerToGodSet)

        return true
    }

    fun givePlayerGodSet(player: Player) {
        val items = mapOf(
            "helmet" to ItemStack(Material.NETHERITE_HELMET, 1),
            "leggings" to ItemStack(Material.NETHERITE_LEGGINGS, 1),
            "chestplate" to ItemStack(Material.NETHERITE_CHESTPLATE, 1),
            "boots" to ItemStack(Material.NETHERITE_BOOTS, 1),
            "sword" to ItemStack(Material.NETHERITE_SWORD),
            "pickaxe" to ItemStack(Material.NETHERITE_PICKAXE),
            "shovel" to ItemStack(Material.NETHERITE_SHOVEL),
            "hoe" to ItemStack(Material.NETHERITE_HOE),
            "axe" to ItemStack(Material.NETHERITE_AXE),
            "shield" to ItemStack(Material.SHIELD),
            "bow" to ItemStack(Material.BOW),
            "crossbow" to ItemStack(Material.CROSSBOW),
            "trident" to ItemStack(Material.TRIDENT),
            "elytra" to ItemStack(Material.ELYTRA),
            "fireworkRockets" to ItemStack(Material.FIREWORK_ROCKET, 64 * 3),
            "arrows" to ItemStack(Material.TIPPED_ARROW, 64 * 4),
        )





        val arrowsMeta = items["arrows"]?.itemMeta as PotionMeta
        arrowsMeta.basePotionData = PotionData(PotionType.POISON, false, true)
        items["arrows"]?.itemMeta = arrowsMeta


        for ((_, item) in items) {
            inner@ for (e in Enchantment::class.java.fields!!) {
                try {
                    val enchant = e.get(null) as Enchantment
                    if (enchant.isCursed) continue@inner
                    item.addEnchantment(enchant, enchant.maxLevel)
//                    item.addEnchantment(enchant, 100)
//                    item.addUnsafeEnchantment(enchant, Short.MAX_VALUE.toInt())
//                    item.addUnsafeEnchantment(enchant, 50)
                } catch (e: IllegalArgumentException) {
                }
            }
            items["bow"]?.addUnsafeEnchantment(Enchantment.ARROW_KNOCKBACK, 150)
            val itype = item.type.name.replace('_', ' ').toLowerCase()
            val meta = item.itemMeta!!
            meta.setDisplayName("$itype of god")
            item.itemMeta = meta
            player.inventory.addItem(item)
        }
    }


}
